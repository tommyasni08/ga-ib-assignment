let arr = ["tomato","broccoli","apple","kale","cabbage","apple"];

function output1() {
  for (let i=0; i < arr.length ; i ++) {
    if (arr[i] !== "apple") {
      console.log(`${arr[i]} is a healthy food, it's definitely worth to eat`);
    }
  }
}

function output2() {
  let result = arr
    .filter(arr => arr !== "apple")
    .forEach(arr => console.log(`${arr} is a healthy food, it's definitely worth to eat`))
}


output1()
console.log("\n");
output2()

const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})

let dt = [
  {
    "name" : "John",
    "status" : "Positive"
  },
  {
    "name" : "Mike",
    "status" : "Suspect"
  },
  {
    "name" : "Wawan",
    "status" : "Positive"
  },
  {
    "name" : "Tommy",
    "status" : "Suspect"
  },
  {
    "name" : "Firman",
    "status" : "Negative"
  },
  {
    "name" : "Ilham",
    "status" : "Negative"
  }
]

function showPositive() {
  console.log("\nPositive");
  console.log("=============");
  let result = dt
    .filter(dt => dt.status == "Positive")
    .forEach(dt => console.log(`${dt.name}`))
  console.log("\n");
}

function showSuspect() {
  console.log("\nSuspect");
  console.log("=============");
  let result = dt
    .filter(dt => dt.status == "Suspect")
    .forEach(dt => console.log(`${dt.name}`))
  console.log("\n");
}

function showNegative() {
  console.log("\nNegative");
  console.log("=============");
  let result = dt
    .filter(dt => dt.status == "Negative")
    .forEach(dt => console.log(`${dt.name}`))
  console.log("\n");
}

function inputOption() {
  console.log(`Please choose your option:`);
  console.log(`1.Positive`);
  console.log(`2.Suspect`);
  console.log(`3.Negative`);
  console.log(`4.Exit`);
  rl.question("Input : ", input => {
    switch(Number(input)) {
      case 1:
        showPositive();
        inputOption();
        break;
      case 2:
        showSuspect();
        inputOption();
        break;
      case 3:
        showNegative();
        inputOption();
        break;
      case 4:
        rl.close();
      default:
        console.log("Sorry, option not available");
        inputOption();
    }
  })
}

rl.on("close", () => {
  process.exit()
})

inputOption();
